from functools import reduce


class Robot:
    """
    Implement logic robot movement
    """

    def __init__(self, instructions: list):
        self.instructions = instructions

    def start_executing(self):
        print('Start moving')

        def show_movements(point_a, point_b):
            print(f'move from {point_a} to {point_b}')
            return point_b

        reduce(show_movements, self.instructions)

        print('Stop moving')
