import time

from django.core.management.base import BaseCommand

from planners.models import RobotRoute
from robots.robot import Robot


class Command(BaseCommand):
    help = 'Run Robot. Robot reads instruction from db and executes them. ' \
           'After 10 seconds he goes to sleep'

    def handle(self, *args, **options):
        print('Start listening ...')
        for _ in range(5):
            queryset = RobotRoute.objects.filter(done=False)
            if queryset:
                route = queryset.first()
                instructions = route.get_instructions()
                r = Robot(instructions)
                r.start_executing()
                route.done = True
                route.save()
            time.sleep(2)
        print('Stop listening')
