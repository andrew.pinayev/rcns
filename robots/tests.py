import sys
from io import StringIO

from django.test import TestCase

from robots.robot import Robot


class BufferWriter(StringIO):

    def __init__(self):
        self.a = list()
        super().__init__()

    def write(self, s: str):
        self.a.append(s)
        super().write(s)


class RobotTestCase(TestCase):

    def test_robot_execute_instructions(self):
        """Catch stdout streaming and assert it"""

        old_stdout = sys.stdout
        b = BufferWriter()
        sys.stdout = b
        instructions = [(12, 5), (8, 5), (8, 13)]
        r = Robot(instructions)
        r.start_executing()
        sys.stdout = old_stdout
        asserted_io = ['Start moving', '\n', 'move from (12, 5) to (8, 5)',
                       '\n', 'move from (8, 5) to (8, 13)', '\n',
                       'Stop moving', '\n']
        self.assertEqual(asserted_io, b.a)
