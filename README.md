# Robot City Navigation System

## Create route

if you are planner and wont create new route.
1. Create csv file with instructions
2. Upload file to api /planners/route/

## Run Robot

To run robot you need run django command `run_robot`.
Robot will read none done instructions and print his movements
in format 

<code>Start moving
 move from (12, 5) to (8, 5)
move from (8, 5) to (8, 13)
Stop moving</code>

## How to write route

In scv file each line is one command.

The first command must be setting start position.

Any command you can write in any letter case.

#### list of commands

- *Start at (0,0)* - set start point, be shure it's first command
- *Turn right* - turn robot right
- *Turn left* - turn robot left
- *go 3* - go 3 blocks
- *go 3 to East* - go 3 blocks to East

## Run tests

`python manage.py tests`

