import csv
import io

from rest_framework import serializers

from planners.RouteParser import RouteParserFromCSV
from planners.models import RobotRoute


class RouteFromCsv(serializers.Serializer):
    file = serializers.FileField()

    def create(self, validated_data):
        decoded_file = validated_data['file'].read().decode()
        reader = csv.reader(io.StringIO(decoded_file), delimiter='\n')

        rp = RouteParserFromCSV()
        rp.parse_csv_file([el[0] for el in reader])
        movement_plan = rp.movement_plan()
        return RobotRoute.objects.create(instructions=movement_plan)
