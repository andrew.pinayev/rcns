from django.test import Client
from django.test import TestCase
from rest_framework import status

from planners.models import RobotRoute


class MyTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_upload_route1(self):
        """Simple test of parsing DSL"""

        with open('planners/test_data/route1.csv') as file:
            response = self.client.post('/planners/route/',
                                        data={'file': file})

            asserted_movement = [(0, 0), (0, 5), (8, 5), (8, 3)]
            instructions = RobotRoute.objects.last().get_instructions()
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(instructions, asserted_movement)

    def test_upload_route2(self):
        """Try to go out of the city"""

        with open('planners/test_data/route2.csv') as file:
            response = self.client.post('/planners/route/',
                                        data={'file': file})

            asserted_movement = [(2, 5), (2, 0), (0, 0), (0, 2), (2, 2)]
            instructions = RobotRoute.objects.last().get_instructions()
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(instructions, asserted_movement)
