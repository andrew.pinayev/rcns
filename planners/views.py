from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from planners.serializers import RouteFromCsv


class Route(APIView):

    def post(self, request):
        """
        Create new route from csv file
        :return: 201 status if created, 400 if something was wrong
        """

        serializer = RouteFromCsv(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED, data={})
