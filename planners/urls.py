from django.urls import path

from planners.views import Route

urlpatterns = [
    path('route/', Route.as_view())
]
