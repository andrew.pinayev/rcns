import re

from rest_framework.exceptions import ValidationError


class Route:
    directions_nesw_format = ('north', 'east', 'south', 'west', 'north')

    def __init__(self):
        self.movements = []
        self.direction = 'south'
        self.set_start_point = False

    def _get_direction_increments(self, direction: str, value: int) -> tuple:
        """
        get increments for movement in each side of NESW(North, East, South,
        West)
        :param direction: nesw direction
        :param value: how many blocks need to go
        :return: (x, y) - increments for sum
        """
        directions_map = {
            'north': (-value, 0),
            'south': (value, 0),
            'west': (0, -value),
            'east': (0, value)
        }
        return directions_map.get(direction)

    def _validate_start(self):
        if self.set_start_point is False:
            raise ValidationError('Need to set start point in first step')

    def _direction_to_nesw_format(self, turn: str) -> str:
        """
        In case when direction is 'left' or 'right' we will translate them to
        North, East, South, West
        :param turn: left or right
        :return: direction in nesw format
        """
        if turn == 'left':
            index = self.directions_nesw_format.index(self.direction)
            return self.directions_nesw_format[index - 1]
        elif turn == 'right':
            index = self.directions_nesw_format.index(self.direction)
            return self.directions_nesw_format[index + 1]

    def _add_coordinates(self, old_coords: tuple, increments: tuple) -> tuple:
        """
        By the rules we can't go to negative coordinates, so we will imagine
        that there are walls on zero lines
        :param old_coords: (x, y) - coordinates from which we will start our
         movement
        :param increments: (x, y) - increments for each coordinate we need to
        sum to go to destination coordinates
        :return: (x, y) - destination coordinates
        """
        x = max(old_coords[0] + increments[0], 0)
        y = max(old_coords[1] + increments[1], 0)
        return x, y

    def start(self, x: int, y: int):
        """
        Set start point in city
        :param x: x coordinate
        :param y: y coordinate
        """
        self.movements.append((int(x), int(y)))

    def turn(self, direction: str):
        """
        Here we can change our direction on the same place
        :param direction: any direction from regular pattern
        """
        if direction not in self.directions_nesw_format:
            direction = self._direction_to_nesw_format(turn=direction)
        self.direction = direction

    def move(self, distance: int, direction: str = None):
        """
        Here we will save robot's instruction to another coordinate
        :param distance: how many need to go
        :param direction: any direction from regular pattern
        :return:
        """
        increments = self._get_direction_increments(
            direction or self.direction,
            int(distance))
        old_coordinates = self.movements[-1]
        self.movements.append(
            self._add_coordinates(old_coordinates, increments))


class RouteParser:
    map = {
        r'start at \((?P<x>[0-9]*),(?P<y>[0-9]*)\)': 'start',
        r'turn (?P<direction>left|right|north|east|south|west)': 'turn',
        r'go (?P<distance>[0-9]*) to (?P<direction>'
        r'left|right|north|east|south|west)': 'move',
        r'go (?P<distance>[0-9]*)': 'move'
    }

    def __init__(self):
        self.route = Route()

    def parse_instruction(self, instruction):
        for instruction_pattern, command in self.map.items():
            m = re.match(instruction_pattern, instruction)
            if m:
                getattr(self.route, command)(**m.groupdict())
                break

    def movement_plan(self):
        return self.route.movements


class RouteParserFromCSV(RouteParser):
    """
    Parser for csv file, but we can create for other formats
    """

    def parse_csv_file(self, instructions: list):
        for instruction in instructions:
            self.parse_instruction(instruction.lower())
