import ast

from django.db import models


class RobotRoute(models.Model):
    instructions = models.TextField(help_text='instructions for robot')
    done = models.BooleanField(default=False,
                               help_text='flag, instruction was read')

    def get_instructions(self):
        return ast.literal_eval(self.instructions)
